﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000007 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000008 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000000F System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000011 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000012 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000013 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000014 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000015 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000018 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000019 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001D System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000021 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000022 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000025 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000026 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000028 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000029 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000002A System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000002B System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000002C System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000002E System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000002F System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000030 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000031 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000032 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000033 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000034 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000035 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000036 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000037 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000038 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000039 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000003A System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000003B System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000003C System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000003D System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000003E System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000003F System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000040 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000041 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000042 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000043 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000044 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000045 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000046 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000047 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000048 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000049 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000004A System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000004B System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000004C System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000004D System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004E System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000004F System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000050 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000051 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000052 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000053 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000054 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000056 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000057 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000058 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000059 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000005A System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000005B T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000005C System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000005D System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[93] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[93] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[26] = 
{
	{ 0x02000004, { 41, 4 } },
	{ 0x02000005, { 45, 9 } },
	{ 0x02000006, { 54, 7 } },
	{ 0x02000007, { 61, 10 } },
	{ 0x02000008, { 71, 1 } },
	{ 0x0200000A, { 72, 3 } },
	{ 0x0200000B, { 77, 5 } },
	{ 0x0200000C, { 82, 7 } },
	{ 0x0200000D, { 89, 3 } },
	{ 0x0200000E, { 92, 7 } },
	{ 0x0200000F, { 99, 4 } },
	{ 0x02000010, { 103, 21 } },
	{ 0x02000012, { 124, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 5 } },
	{ 0x06000006, { 15, 2 } },
	{ 0x06000007, { 17, 1 } },
	{ 0x06000008, { 18, 3 } },
	{ 0x06000009, { 21, 2 } },
	{ 0x0600000A, { 23, 4 } },
	{ 0x0600000B, { 27, 3 } },
	{ 0x0600000C, { 30, 1 } },
	{ 0x0600000D, { 31, 3 } },
	{ 0x0600000E, { 34, 2 } },
	{ 0x0600000F, { 36, 5 } },
	{ 0x0600002D, { 75, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[126] = 
{
	{ (Il2CppRGCTXDataType)2, 19379 },
	{ (Il2CppRGCTXDataType)3, 15794 },
	{ (Il2CppRGCTXDataType)2, 19380 },
	{ (Il2CppRGCTXDataType)2, 19381 },
	{ (Il2CppRGCTXDataType)3, 15795 },
	{ (Il2CppRGCTXDataType)2, 19382 },
	{ (Il2CppRGCTXDataType)2, 19383 },
	{ (Il2CppRGCTXDataType)3, 15796 },
	{ (Il2CppRGCTXDataType)2, 19384 },
	{ (Il2CppRGCTXDataType)3, 15797 },
	{ (Il2CppRGCTXDataType)2, 19385 },
	{ (Il2CppRGCTXDataType)3, 15798 },
	{ (Il2CppRGCTXDataType)3, 15799 },
	{ (Il2CppRGCTXDataType)2, 14971 },
	{ (Il2CppRGCTXDataType)3, 15800 },
	{ (Il2CppRGCTXDataType)2, 19386 },
	{ (Il2CppRGCTXDataType)3, 15801 },
	{ (Il2CppRGCTXDataType)3, 15802 },
	{ (Il2CppRGCTXDataType)2, 19387 },
	{ (Il2CppRGCTXDataType)3, 15803 },
	{ (Il2CppRGCTXDataType)3, 15804 },
	{ (Il2CppRGCTXDataType)2, 14987 },
	{ (Il2CppRGCTXDataType)3, 15805 },
	{ (Il2CppRGCTXDataType)2, 19388 },
	{ (Il2CppRGCTXDataType)2, 19389 },
	{ (Il2CppRGCTXDataType)2, 14988 },
	{ (Il2CppRGCTXDataType)2, 19390 },
	{ (Il2CppRGCTXDataType)2, 14990 },
	{ (Il2CppRGCTXDataType)2, 19391 },
	{ (Il2CppRGCTXDataType)3, 15806 },
	{ (Il2CppRGCTXDataType)2, 14993 },
	{ (Il2CppRGCTXDataType)2, 14995 },
	{ (Il2CppRGCTXDataType)2, 19392 },
	{ (Il2CppRGCTXDataType)3, 15807 },
	{ (Il2CppRGCTXDataType)2, 19393 },
	{ (Il2CppRGCTXDataType)3, 15808 },
	{ (Il2CppRGCTXDataType)3, 15809 },
	{ (Il2CppRGCTXDataType)2, 19394 },
	{ (Il2CppRGCTXDataType)2, 15000 },
	{ (Il2CppRGCTXDataType)2, 19395 },
	{ (Il2CppRGCTXDataType)2, 15002 },
	{ (Il2CppRGCTXDataType)3, 15810 },
	{ (Il2CppRGCTXDataType)3, 15811 },
	{ (Il2CppRGCTXDataType)2, 15005 },
	{ (Il2CppRGCTXDataType)3, 15812 },
	{ (Il2CppRGCTXDataType)3, 15813 },
	{ (Il2CppRGCTXDataType)2, 15014 },
	{ (Il2CppRGCTXDataType)2, 19396 },
	{ (Il2CppRGCTXDataType)3, 15814 },
	{ (Il2CppRGCTXDataType)3, 15815 },
	{ (Il2CppRGCTXDataType)2, 15016 },
	{ (Il2CppRGCTXDataType)2, 19271 },
	{ (Il2CppRGCTXDataType)3, 15816 },
	{ (Il2CppRGCTXDataType)3, 15817 },
	{ (Il2CppRGCTXDataType)3, 15818 },
	{ (Il2CppRGCTXDataType)2, 15023 },
	{ (Il2CppRGCTXDataType)2, 19397 },
	{ (Il2CppRGCTXDataType)3, 15819 },
	{ (Il2CppRGCTXDataType)3, 15820 },
	{ (Il2CppRGCTXDataType)3, 15215 },
	{ (Il2CppRGCTXDataType)3, 15821 },
	{ (Il2CppRGCTXDataType)3, 15822 },
	{ (Il2CppRGCTXDataType)2, 15032 },
	{ (Il2CppRGCTXDataType)2, 19398 },
	{ (Il2CppRGCTXDataType)3, 15823 },
	{ (Il2CppRGCTXDataType)3, 15824 },
	{ (Il2CppRGCTXDataType)3, 15825 },
	{ (Il2CppRGCTXDataType)3, 15826 },
	{ (Il2CppRGCTXDataType)3, 15827 },
	{ (Il2CppRGCTXDataType)3, 15221 },
	{ (Il2CppRGCTXDataType)3, 15828 },
	{ (Il2CppRGCTXDataType)3, 15829 },
	{ (Il2CppRGCTXDataType)2, 19399 },
	{ (Il2CppRGCTXDataType)3, 15830 },
	{ (Il2CppRGCTXDataType)3, 15831 },
	{ (Il2CppRGCTXDataType)2, 19400 },
	{ (Il2CppRGCTXDataType)3, 15832 },
	{ (Il2CppRGCTXDataType)2, 19401 },
	{ (Il2CppRGCTXDataType)3, 15833 },
	{ (Il2CppRGCTXDataType)3, 15834 },
	{ (Il2CppRGCTXDataType)3, 15835 },
	{ (Il2CppRGCTXDataType)2, 15064 },
	{ (Il2CppRGCTXDataType)3, 15836 },
	{ (Il2CppRGCTXDataType)2, 15072 },
	{ (Il2CppRGCTXDataType)3, 15837 },
	{ (Il2CppRGCTXDataType)2, 19402 },
	{ (Il2CppRGCTXDataType)2, 19403 },
	{ (Il2CppRGCTXDataType)3, 15838 },
	{ (Il2CppRGCTXDataType)3, 15839 },
	{ (Il2CppRGCTXDataType)3, 15840 },
	{ (Il2CppRGCTXDataType)3, 15841 },
	{ (Il2CppRGCTXDataType)3, 15842 },
	{ (Il2CppRGCTXDataType)3, 15843 },
	{ (Il2CppRGCTXDataType)2, 15088 },
	{ (Il2CppRGCTXDataType)2, 19404 },
	{ (Il2CppRGCTXDataType)3, 15844 },
	{ (Il2CppRGCTXDataType)3, 15845 },
	{ (Il2CppRGCTXDataType)2, 15092 },
	{ (Il2CppRGCTXDataType)3, 15846 },
	{ (Il2CppRGCTXDataType)2, 19405 },
	{ (Il2CppRGCTXDataType)2, 15102 },
	{ (Il2CppRGCTXDataType)2, 15100 },
	{ (Il2CppRGCTXDataType)2, 19406 },
	{ (Il2CppRGCTXDataType)3, 15847 },
	{ (Il2CppRGCTXDataType)2, 19407 },
	{ (Il2CppRGCTXDataType)3, 15848 },
	{ (Il2CppRGCTXDataType)3, 15849 },
	{ (Il2CppRGCTXDataType)3, 15850 },
	{ (Il2CppRGCTXDataType)2, 15106 },
	{ (Il2CppRGCTXDataType)3, 15851 },
	{ (Il2CppRGCTXDataType)3, 15852 },
	{ (Il2CppRGCTXDataType)2, 15109 },
	{ (Il2CppRGCTXDataType)3, 15853 },
	{ (Il2CppRGCTXDataType)1, 19408 },
	{ (Il2CppRGCTXDataType)2, 15108 },
	{ (Il2CppRGCTXDataType)3, 15854 },
	{ (Il2CppRGCTXDataType)1, 15108 },
	{ (Il2CppRGCTXDataType)1, 15106 },
	{ (Il2CppRGCTXDataType)2, 19409 },
	{ (Il2CppRGCTXDataType)2, 15108 },
	{ (Il2CppRGCTXDataType)3, 15855 },
	{ (Il2CppRGCTXDataType)3, 15856 },
	{ (Il2CppRGCTXDataType)3, 15857 },
	{ (Il2CppRGCTXDataType)2, 15107 },
	{ (Il2CppRGCTXDataType)3, 15858 },
	{ (Il2CppRGCTXDataType)2, 15120 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	93,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	26,
	s_rgctxIndices,
	126,
	s_rgctxValues,
	NULL,
};
