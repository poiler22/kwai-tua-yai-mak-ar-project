﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.iOS;
using UnityEngine.SceneManagement;

public class Destroy : MonoBehaviour {
    // Start is called before the first frame update
    public Rigidbody rigid;
    public Animator animator;

    private float timer;
    private float gameTime = 150.0f;
    public TMPro.TextMeshProUGUI tmp;
    public TMPro.TextMeshProUGUI count;
    //public TMPro.TextMeshProUGUI result;
    public int score = 0;
    public Animator transition;

    void Start ()
    {
        timer = gameTime;
    }

   

    // Update is called once per frame
    void Update ()
    {

        timer -= Time.deltaTime;
        count.text = "Time: " + Mathf.Round (timer);
        tmp.text = "Score : " + score;

        //if (timer<0)
        //{
        //    StartCoroutine(gameover());
        //}

       
    }

    IEnumerator gameover()
    {
        transition.SetBool("GameOver", true);
        yield return new WaitForSeconds(2f);
        if (score >= 15)
        {

            // win.text = ("YOU WON!!");
            yield return new WaitForSeconds(2f);
            SceneManager.LoadScene("Wonn");
        }

        if (score < 15)
        {
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("Gameoverr");

        }



    }


}

